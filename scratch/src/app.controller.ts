import { Controller, Get } from "@nestjs/common";

@Controller("/")
export class AppController {
    @Get("/hi")
    getRootRoute() {
        return "hi there!"
    }

    @Get("/bye")
    byeThere() {
        return "bye!!"
    }

    @Get("/")
    default() {
        return "i am default route"
    }
}