import { Module } from "@nestjs/common";
import { AppController } from "./app.controller";

@Module(({
    controllers: [AppController] //list all controller this module will use so it can automatically create instances of the controllers listed

}))
export class AppModule { }