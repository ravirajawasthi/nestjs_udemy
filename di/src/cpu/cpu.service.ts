import { Injectable } from '@nestjs/common';
import { PowerService } from 'src/power/power.service';

@Injectable()
export class CpuService {
  constructor(private powerService: PowerService) {}

  compute(a: number, b: number) {
    console.log('Getting 10 watts from power module');
    this.powerService.supportPower(10);
    return a + b;
  }
}
