import { Injectable } from '@nestjs/common';

@Injectable()
export class PowerService {
  supportPower(watts: number) {
    console.log(`supplying ${watts} of power`);
    return watts;
  }
}
