import { Injectable } from "@nestjs/common";
import { writeFile, readFile } from "fs/promises";

@Injectable()
export class MessageRepository {
    async findOne(id: string) {
        const db = await readFile('messages.json', 'utf8')
        const messages = JSON.parse(db)

        return messages[id]
    }

    async findAll() {
        const db = await readFile('messages.json', 'utf8')
        return JSON.parse(db)
    }

    async create(content: string) {
        const db = await readFile('messages.json', 'utf8')
        const messages = JSON.parse(db)

        const id = Math.floor(Math.random() * 10000) + 1

        messages[id] = {
            id,
            content
        }

        await writeFile("messages.json", JSON.stringify(messages), "utf8")

    }
}