import { NotFoundException } from '@nestjs/common';
import { Controller, Get, Post, Body, Param } from '@nestjs/common';
import { CreateMessageDto } from './dtos/create-message.dto';
import { MessageService } from './messages.service';

@Controller('/messages')
export class MessagesController {
	constructor(public messageService: MessageService) {

	}

	@Get('/')
	listMessages() {
		return this.messageService.findAll()
	}

	@Post()
	createMessage(@Body() body: CreateMessageDto) {
		return this.messageService.create(body.content);
	}

	@Get('/:id')
	async getMessages(@Param('id') id: string) {
		const message = await this.messageService.findOne(id)
		if (message) {
			return message
		} else {
			throw new NotFoundException("Message Not Found")
		}
	}
}
