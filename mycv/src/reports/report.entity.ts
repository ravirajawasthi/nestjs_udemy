import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from '../users/user.entity';

@Entity()
export class Report {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: false })
  approved: boolean;

  @Column()
  price: number;

  @Column()
  make: string;

  @Column()
  model: string;

  @Column()
  year: number;

  @Column()
  lng: number;

  @Column()
  lat: number;

  @Column()
  mileage: number;

  //first argument is telling nest to understand which class to be associated with
  //why we are wrapping it in a function is because by the time its executed then User class has already been initialized

  //Second argument is not useful now but suppose if report was associated with 2 different users (user who submit and some other user who approve) then it will help to mitigate that problem (dont know how)
  @ManyToOne(() => User, (user) => user.reports)
  user: User;
}
