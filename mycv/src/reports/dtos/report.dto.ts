import { Expose, Transform } from 'class-transformer';

export class ReportDto {
  @Expose()
  id: number;
  @Expose()
  price: number;
  @Expose()
  year: number;
  @Expose()
  lng: number;
  @Expose()
  lat: number;
  @Expose()
  mileage: number;

  @Expose()
  approved: boolean;

  //this will get the user obj and get its user.id and set it to userId
  @Transform(({ obj }) => obj.user.id)
  @Expose()
  userId: number;
}
