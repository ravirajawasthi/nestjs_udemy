import { createParamDecorator, ExecutionContext } from '@nestjs/common';

//ExecutionContext is just a wrapper around the incoming request. it could be websocket http, grpc etc.
export const CurrentUser = createParamDecorator(
  (data: never, context: ExecutionContext) => {
    const request = context.switchToHttp().getRequest();
    return request.currentUser;
  },
);
