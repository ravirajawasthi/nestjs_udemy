import { Test, TestingModule } from '@nestjs/testing';
import { UsersController } from './users.controller';
import { AuthService } from './auth.service';
import { UsersService } from './users.service';
import { User } from './user.entity';

describe('UsersController', () => {
  let controller: UsersController;
  let fakeAuthService: Partial<AuthService>;
  let fakeUserService: Partial<UsersService>;
  beforeEach(async () => {
    fakeAuthService = {
      signin: (email: string, password: string) => {
        return Promise.resolve({ id: 1, email, password } as User);
      },
      // signup: () => {},
    };
    fakeUserService = {
      find: (email: string) =>
        Promise.resolve([{ id: 1, email, password: 'dummyPassword' } as User]),
      findOne: (id: number) =>
        Promise.resolve({
          id,
          email: 'dummy@gmail.com',
          password: 'password',
        } as User),
    };
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        { provide: UsersService, useValue: fakeUserService },
        {
          provide: AuthService,
          useValue: fakeAuthService,
        },
      ],
    }).compile();

    controller = module.get<UsersController>(UsersController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('findAllUsers gives all users with correct email id', async () => {
    const users = await controller.findAllUsers('dummy@gmail.com');
    expect(users.length).toBe(1);
    expect(users[0].email).toBe('dummy@gmail.com');
  });

  it('findUser returns user with given id', async () => {
    const user = await controller.findUser('99');
    expect(user).toBeDefined();
  });

  it('findUser throws error when user with id not present', (done) => {
    fakeUserService.findOne = (id: number) => Promise.resolve(null);
    controller
      .findUser('99')
      .then()
      .catch((err) => done());
  });

  it('signing user and update session', async () => {
    const session = { userId: -10 };
    const user = await controller.signinUser(
      {
        email: 'dummy@gmail.com',
        password: 'dummypass',
      },
      session,
    );

    expect(user.id).toBe(1);
    expect(session.userId).toBe(1);
  });
});
