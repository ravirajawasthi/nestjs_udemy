import {
  BadRequestException,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { randomBytes, scrypt as _scrypt } from 'crypto';
import { promisify } from 'util';
import { UsersService } from './users.service';

const scrypt = promisify(_scrypt);

@Injectable()
export class AuthService {
  constructor(private userService: UsersService) {}
  async signup(email: string, password: string) {
    const users = await this.userService.find(email);

    if (users.length) {
      throw new BadRequestException('Email id already in use');
    }
    const salt = randomBytes(64).toString('hex');

    const hash = (await scrypt(password, salt, 512)) as Buffer;

    password = salt + '.' + hash.toString('hex');

    return this.userService.create(email, password);
  }

  async signin(email: string, password: string) {
    const [user] = await this.userService.find(email);
    if (!user) {
      throw new NotFoundException('user does not exist');
    }

    const [salt, storedHash] = user.password.split('.');

    const hash = ((await scrypt(password, salt, 512)) as Buffer).toString(
      'hex',
    );

    if (hash === storedHash) {
      return user;
    }

    throw new UnauthorizedException('Invalid Credentials');
  }
}
