import { UsersService } from './users.service';
import { User } from './user.entity';
import { Test } from '@nestjs/testing';
import { AuthService } from './auth.service';

describe('AuthService', () => {
  let service: AuthService;
  const users: User[] = [];
  const fakeUserService: Partial<UsersService> = {
    find: (email) => {
      const filteredUsers = users.filter((user) => user.email === email);
      return Promise.resolve(filteredUsers);
    },
    create: (email: string, password: string) => {
      const newUser = {
        id: Math.floor(Math.random() * 9999),
        email,
        password,
      } as User;
      users.push(newUser);
      return Promise.resolve(newUser);
    },
  };
  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide: UsersService,
          useValue: fakeUserService,
        },
      ],
    }).compile();

    service = module.get(AuthService);
  });

  it('can create an instance of auth service', async () => {
    expect(service).toBeDefined();
  });

  it('created a new user with hashed password with salt', async () => {
    const user = await service.signup('dummy@gmail.com', 'PlainTextBad');
    expect(user.password).not.toEqual('PlainTextBad');
    const [salt, hashedPassword] = user.password.split('.');
    expect(salt).toBeDefined();
    expect(salt.length).not.toEqual(0);
    expect(hashedPassword).toBeDefined();
    expect(hashedPassword.length).not.toEqual(0);
  });

  it('gives error when user signs up with email that is already in use', (done) => {
    service
      .signup('dummy@gmail.com', 'dummy')
      .then()
      .catch((err) => {
        done();
      });
  });

  it('throws error when signin is used with an non existing email', (done) => {
    service
      .signin('RealDummy@gmail.com', 'password')
      .then()
      .catch((err) => {
        expect(err.name).toEqual('NotFoundException');
        expect(err.message).toEqual('user does not exist');
        done();
      });
  });

  it('throws error when invald argument is passed on', (done) => {
    service
      .signin('dummy@gmail.com', 'fakeDummy')
      .then()
      .catch((err) => done());
  });

  it('returns user if password is correct', (done) => {
    service
      .signup('dummyGood@gmail.com', 'dummy')
      .then(() =>
        service.signin('dummyGood@gmail.com', 'dummy').then(() => done()),
      );
  });
});
