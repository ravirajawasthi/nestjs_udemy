import {
  AfterInsert,
  AfterUpdate,
  BeforeRemove,
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Report } from '../reports/report.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  email: string;

  @Column({ default: true })
  admin: boolean;

  @Column()
  // @Exclude() // remove this property when sending request to user
  password: string;

  @OneToMany(() => Report, (report) => report.user)
  reports: Report[];

  @AfterInsert()
  logInsert() {
    console.log(`new user inserted with id ${this.id}`);
  }

  @AfterUpdate()
  logUpdate() {
    console.log(`user with id : ${this.id} updated`);
  }

  @BeforeRemove()
  logRemove() {
    console.log(`user with id : ${this.id} removed`);
  }
}
