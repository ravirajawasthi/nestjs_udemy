import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';

@Injectable()
export class UsersService {
  constructor(@InjectRepository(User) private repo: Repository<User>) {}
  create(email: string, password: string) {
    const user = this.repo.create({ email, password }); //if we dont create the user entity then no validation is executed because no hooks are executed
    return this.repo.save(user);
  }

  findOne(id: number) {
    if (!id) {
      throw new BadRequestException('Invalid Request');
    }
    return this.repo.findOne(id);
  }

  find(email: string) {
    return this.repo.find({ email });
  }

  async update(id: number, attrs: Partial<User>) {
    const foundUser = await this.findOne(id); //findOne returns a promise, so we can use await here
    if (!foundUser) {
      throw new Error(`No User found with id ${id}`);
    }
    Object.assign(foundUser, attrs);
    return this.repo.save(foundUser);
  }

  async remove(id: number) {
    const foundUser = await this.findOne(id);
    if (!foundUser) {
      throw new Error('User not found');
    }
    return this.repo.remove(foundUser);
  }

  async clearUserTable() {
    const allUsers = await this.repo.find();
    await this.repo.remove(allUsers);
    return 'Users Database Cleared';
  }
}
