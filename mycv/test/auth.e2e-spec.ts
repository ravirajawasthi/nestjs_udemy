import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';

describe('Authentication System', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('it handles a signup request', () => {
    const requestEmail = 'fjohaodfghfdghhjf@gmail.com';
    return request(app.getHttpServer())
      .post('/auth/signup')
      .send({
        email: requestEmail,
        password: 'apkfjoafhjk',
      })
      .expect(201)
      .then((res) => {
        const { id, email } = res.body;
        expect(id).toBeDefined();
        expect(email).toEqual(requestEmail);
      });
  });

  it('it handles a signup request', () => {
    const requestEmail = 'fjohaodadadfghfdghhjf@gmail.com';
    return request(app.getHttpServer())
      .post('/auth/signup')
      .send({
        email: requestEmail,
        password: 'apkfjoafhjk',
      })
      .expect(201)
      .then((res) => {
        const { id, email } = res.body;
        expect(id).toBeDefined();
        expect(email).toEqual(requestEmail);
      });
  });

  it('signup as new user and get current user', async () => {
    const requestEmail = 'ofhsdopgh@gmail.com';
    const res = await request(app.getHttpServer())
      .post('/auth/signup')
      .send({ email: requestEmail, password: 'pfkjsapofdjh' })
      .expect(201);

    const cookie = res.get('Set-Cookie');

    const userResp = await request(app.getHttpServer())
      .get('/auth/whoami')
      .set('Cookie', cookie)
      .expect(200);

    expect(userResp.body.email).toBe(requestEmail);
  });
});
